<?php
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

$app->get('/api/gpstracker/tracker',  function(Request $request, Response $response){
    $tracker_imei = $request->getParam('tracker_imei');
    $tracker_lat = $request->getParam('tracker_lat');
    $tracker_lon = $request->getParam('tracker_lon');
    $tracker_speed = $request->getParam('tracker_speed');

    $sql = "call st_gpstracker('".$tracker_imei."','".$tracker_lat."','".$tracker_lon."','".$tracker_speed."');";

    try{
      $db = new db();
      $db = $db->conectDB();
      $resultado = $db->query($sql);

      if($resultado->rowCount() > 0){
        $result = $resultado->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($result);
      }else{
        echo json_encode("Error Save Position");
      }

      $resultado = null;
      $db = null;

    //	echo json_encode("LogIn Exitoso: ".$usuario);
    }catch(PDOException $e){
      echo '{"error" : {"text" : '.$e->getMessage().'} }';
    }
  });

  $app->get('/api/gpstracker/historytracker',  function(Request $request, Response $response){
    $tracker_imei= $request->getParam('imei');
    $tracker_fini= $request->getParam('fechaini');
    $tracker_ffin= $request->getParam('fechafin');

    $sql = "call st_histoytracker('".$tracker_imei."','".$tracker_fini."','".$tracker_ffin."');";

    try{
      $db = new db();
      $db = $db->conectDB();
      $resultado = $db->query($sql);

      if($resultado->rowCount() > 0){
        $result = $resultado->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($result);
      }else{
        echo json_encode("Error");
      }

      $resultado = null;
      $db = null;

    //	echo json_encode("LogIn Exitoso: ".$usuario);
    }catch(PDOException $e){
      echo '{"error" : {"text" : '.$e->getMessage().'} }';
    }
  });

  $app->post('/api/users/login',  function(Request $request, Response $response){
    $urs_cedula = $request->getParam('usrcedula');
    $urs_password = $request->getParam('usrpassword');

    $sql = "call st_login(".$urs_cedula.",'".$urs_password."');";

    try{
      $db = new db();
      $db = $db->conectDB();
      $resultado = $db->query($sql);
    
      if($resultado->rowCount() > 0){
        $result = $resultado->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($result);
      }else{
        echo json_encode("Error LogIn");
      }

      $resultado = null;
      $db = null;

    //	echo json_encode("LogIn Exitoso: ".$usuario);
    }catch(PDOException $e){
      echo '{"error" : {"text" : '.$e->getMessage().'} }';
    }
  });

  $app->get('/api/users/setuser',  function(Request $request, Response $response){
    $tracker_cedula= $request->getParam('cedula');

    $sql = "call st_setuser(".$tracker_cedula.");";

    try{
      $db = new db();
      $db = $db->conectDB();
      $resultado = $db->query($sql);

      if($resultado->rowCount() > 0){
        $result = $resultado->fetchAll(PDO::FETCH_OBJ);
        echo json_encode($result);
      }else{
        echo json_encode("Error");
      }

      $resultado = null;
      $db = null;

    //	echo json_encode("LogIn Exitoso: ".$usuario);
    }catch(PDOException $e){
      echo '{"error" : {"text" : '.$e->getMessage().'} }';
    }
  });

?>
