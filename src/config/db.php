<?php
class db{
		private $dbHost = '3.129.11.64';
		private $dbUser = 'root';
		private $dbPass = 'test';
		private $dbName = 'gps_tracker';
	

		public function conectDB(){
			$mysqlConnect = "mysql:host=$this->dbHost;dbname=$this->dbName";
			$dbConnecion = new PDO($mysqlConnect, $this->dbUser, $this->dbPass);
			$dbConnecion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			return $dbConnecion;
		}
}
?>