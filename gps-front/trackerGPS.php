<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET");

$tracker_imei = (isset($_GET['tracker_imei']) && $_GET['tracker_imei']) ? $_GET['tracker_imei'] : '0';
$tracker_lat =  (isset($_GET['tracker_lat']) && $_GET['tracker_lat']) ? $_GET['tracker_lat'] : '0';
$tracker_lon =  (isset($_GET['tracker_lon']) && $_GET['tracker_lon']) ? $_GET['tracker_lon'] : '0';
$tracker_speed =  (isset($_GET['tracker_speed']) && $_GET['tracker_speed']) ? $_GET['tracker_speed'] : '0';


if($tracker_imei && $tracker_lat && $tracker_lon && $tracker_speed){   

    $url = 'http://localhost/public/api/gpstracker/tracker?tracker_imei='.$tracker_imei.'&tracker_lat='.$tracker_lat.'&tracker_lon='.$tracker_lon.'&tracker_speed='.$tracker_speed;
  
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    
    //$response = json_decode($response);
    echo $response;
    curl_close($ch);
}else{     
    http_response_code(404);     
    echo "No position saved";
} 