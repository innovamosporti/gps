<!DOCTYPE html>
<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">


     <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">     
     <!-- Font Awesome -->     <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">    
     <!-- NProgress -->     <link href="vendors/nprogress/nprogress.css" rel="stylesheet">    
      <!-- bootstrap-daterangepicker -->     <link href="vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">    
       <!-- bootstrap-datetimepicker -->     <link href="vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">     
       <!-- Ion.RangeSlider -->     <link href="vendors/normalize-css/normalize.css" rel="stylesheet">     
       <link href="vendors/ion.rangeSlider/css/ion.rangeSlider.css" rel="stylesheet">     
       <link href="vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet">     
       <!-- Bootstrap Colorpicker -->     <link href="vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">     
       <!-- iCheck -->     <link href="vendors/iCheck/skins/flat/green.css" rel="stylesheet">    
       <!-- Datatables -->     <link href="vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">    
       <link href="vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">     
       <link href="vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">     
       <link href="vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">     
       <link href="vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">      
       <!-- PNotify -->     <link href="vendors/pnotify/dist/pnotify.css" rel="stylesheet">     
       <link href="vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">     
       <link href="vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">   
       <!-- Dropzone.js -->
    <link href="vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet"> 
    <!-- Switchery -->
    <link href="vendors/switchery/dist/switchery.min.css" rel="stylesheet"> 
   <!-- Custom Theme Style -->     <link href="build/css/custom.min.css" rel="stylesheet"> 
    
    <title>GPS Tracker</title>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="#" class="site_title"><span>GPS Tracker</span></a>
                </div>
                <div class="clearfix"></div>
                
                <div class="profile clearfix">
                    <div class="profile_pic">
                        <img src="assets/user.png" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Bienvenido</span>
                        <h2></h2>
                    </div>
                </div>
                    <!-- /menu profile quick info -->

                    <br />
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <h3></h3>
                                <ul class="nav side-menu">
                            
                                </ul>
                        </div>
                    </div>

                    <!-- /menu footer buttons --> 
                    <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Logout" href="index.html">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>

            </div>
        </div>
                <!-- top navigation -->
                <div class="top_nav">
                        <div class="nav_menu">
                            <nav>
                                <ul class="nav navbar-nav navbar-right">
                                <div class="row" style="display: inline-block;">
                                    <div class="tile_count">
                                        <div class="col-md-7 col-sm-7  tile_stats_count">
                                            <span class="count_top"><i class="fa fa-wifi"></i>Dispositivo IMEI</span>
                                            <div class="count"><div id="imei"></div></div>
                                        </div>
                                        <div class="col-md-5 col-sm-5 tile_stats_count">
                                            <span class="count_top"><i class="fa fa-phone"></i>SIM Card</span>
                                            <div class="count"><div id="sim"></div></div>
                                        </div>
                                    </div>
                                </div>
                                    
                                    <li class="">
                                        <a href="#" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            <div id="usuario"></div>
                                        <span class=" fa fa-user"></span>
                                        </a>
                                    
                                    </li>
                                </ul>
                
                            </nav>
                    </div>
                    </div>
                    <!-- /top navigation -->

                    <!-- page content -->
                <div class="right_col" role="main">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                <div id="message"></div>
                                    <div class="x_title">
                                        <h2>Buscar</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li>
                                                <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">         
                                        <div class="col-md-4 col-sm-6  has-feedback">
                                            <div class="form-group">
                                                <input type="text" class="form-control has-feedback-left" id="fecha_ini" placeholder="Fecha Inicial">
                                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6  has-feedback">
                                            <div class="form-group">
                                                <input type="text" class="form-control has-feedback-left" id="fecha_fin" placeholder="Fecha Final">
                                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-4 col-sm-6 has-feedback"> 
                                            <div class="form-group">
                                                <button class="btn btn-primary" type="reset" id="Reiniciar">Reiniciar</button> 
                                                <button type="button" class="btn btn-success" id="Buscar">Buscar</button> 
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>

                        
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Geolocalización de IPS</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li>
                                                <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">         
                                        <div style="width: 100%">
                                            <iframe id="Mapa" src="mapa.php" width="100%" height="610"></iframe>
                                        </div>
                                    </div> 
                                </div>
                            </div>

                        </div>
                    </div>
                    
                    <!-- /page content -->

                    <!-- footer content -->

                    <!-- /footer content -->

      </div>
    </div>


<!-- jQuery --><script src="vendors/jquery/dist/jquery.min.js"></script>     
<!-- script src="../js/jquery-2.1.0.js"></script -->    
<!-- Bootstrap --> <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>     
<!-- FastClick -->     <script src="vendors/fastclick/lib/fastclick.js"></script>     
<!-- NProgress -->     <script src="vendors/nprogress/nprogress.js"></script>     
<!-- bootstrap-daterangepicker -->     <script src="vendors/moment/min/moment.min.js"></script>     
<script src="vendors/bootstrap-daterangepicker/daterangepicker.js"></script>     
<!-- bootstrap-datetimepicker -->         <script src="vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>     
<script src="vendors/bootstrap-datetimepicker/build/js/es.js"></script>     
<!-- Ion.RangeSlider -->     <script src="vendors/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>     
<!-- Bootstrap Colorpicker -->     <script src="vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>     
<!-- jquery.inputmask -->     <script src="vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>     
<!-- jQuery Knob -->     <script src="vendors/jquery-knob/dist/jquery.knob.min.js"></script>     
<!-- validator -->     <script src="vendors/validator/validator.js"></script>     
<!-- Block de Pagina -->     <script src="vendors/block/jquery.blockUI.js"></script>        
<!-- Datatables -->     <script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>     
<script src="vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>    
 <script src="vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>     
 <script src="vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>     
 <script src="vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>     
 <script src="vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>     
 <script src="vendors/datatables.net-buttons/js/buttons.print.min.js"></script>     
 <script src="vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>     
 <script src="vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>     
 <script src="vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>     
 <script src="vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>     
 <script src="vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>      
 <!-- PNotify -->     <script src="vendors/pnotify/dist/pnotify.js"></script>     
 <script src="vendors/pnotify/dist/pnotify.buttons.js"></script>     
 <script src="vendors/pnotify/dist/pnotify.nonblock.js"></script> 
 <!-- Dropzone.js -->
    <script src="vendors/dropzone/dist/min/dropzone.min.js"></script> 
    <!-- Switchery -->
  <script src="vendors/switchery/dist/switchery.min.js"></script>       
  <!-- Custom Theme Scripts -->     <script src="build/js/custom.min.js"></script>
  
    <script>
        $(document).ready(function(){ 
            
            $('#fecha_ini').datetimepicker({
                format: 'YYYY-MM-DD',
                locale: 'es',
            });

            $('#fecha_fin').datetimepicker({
                format: 'YYYY-MM-DD',
                locale: 'es',
            });
        });
    </script>
    <script src="js/session.js"></script>
    <script src="js/dashboard.js"></script>


  </body>
  </html>