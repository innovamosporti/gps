      $(document).ready(function(){
        sessionStorage.clear();
        //código jQuery adicional
        $("#btnLogIn").click(function(){
          $("#MsgLogIn").html('<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i');
          var username = $("#Usr").val();
          var pwd = $("#Pass").val();

          console.log(username + pwd);
          data = {
            usrcedula: username,
            usrpassword: pwd
          }
          var server = "3.129.11.64";
          var port = "80";
          $.ajax("http://"+server+":"+port+"/public/api/users/login", {
            data : JSON.stringify(data),
            contentType : 'application/json',
            type : 'POST',
            success:function(data){
              
              //console.log("1: "+data[1]);
              var response = JSON.parse(data);
              //console.log(response[0].message);
              var status = response[0].message
              
              if(status == "true"){
                $(function(){
                    $("#MsgLogIn").html("");
                    $.session.set("cedula", username);
                    $.session.set("session", 1);
                    $(location).prop('href', 'dashboard.php');
                  });
              }else{
                $("#MsgLogIn").html('<div class="alert alert-warning alert-dismissible fade in" role="alert"> Error LogIn </div>');
              }

              
              
            },
            error:function(xhr, status, textStatus){
              console.log("2:"+xhr.status);
              var arr = xhr.responseText;
              $("#MsgLogIn").html(xhr.responseText);
              console.log("2:"+xhr.responseText);
            }
          });


        });
      });
 