$(document).ready(function(){ 
    var session = $.session.get("session");
    
    if(session == 1){
        var cedula = $.session.get("cedula");
        var server = "3.129.11.64";
        var port = "80";

        $.ajax("http://"+server+":"+port+"/public/api/users/setuser?cedula="+cedula, {
            contentType : 'application/json',
            type : 'GET',
            success(data) {
            
            var response = JSON.parse(data);
                if(response[0].pago ==1){
                    $.session.set("imei", response[0].imei);
                    $("#usuario").html(response[0].usuario);
                    $("#imei").html(response[0].imei);
                    $("#sim").html(response[0].sim);
                }else{
                    $("#message").html('<div class="alert alert-danger alert-dismissible " role="alert"> \
                    <strong>Error!</strong> No presenta pago. \
                    </div>');
                    $("#Buscar").attr("disabled", true);
                    $("#fecha_ini").attr("disabled", true);
                    $("#fecha_fin").attr("disabled", true);
                }
            },
            error(request, status, error) {
            console.log(error)
            }
        });

        $("#Buscar").click(function(){
            var fecha_ini = $("#fecha_ini").val();
            var fecha_fin = $("#fecha_fin").val();
            var imei = $.session.get("imei");

            if(fecha_ini <= fecha_fin){
                $("#fecha_ini").val('');
                $("#fecha_fin").val('');
                $("#message").html('');
                $("#Mapa").attr('src','mapa.php?imei='+imei+'&fechaini='+fecha_ini+'&fechafin='+fecha_fin);
            }else{
                $("#message").html('<div class="alert alert-danger alert-dismissible " role="alert"> \
                    <strong>Error!</strong> Fechas Invalidad. </div>');
            }
        });

        $("#Reiniciar").click(function(){
            $("#fecha_ini").val('');
            $("#fecha_fin").val('');
        });

    }else{
        $(location).prop('href', 'index.html');
    }

});