<?php
	require('Config.php');

	/**
	 * 
	 */
	class Conexion{
		
		protected $conexion_db;

		public function Conexion(){
			$this->conexion_db = new mysqli(DB_HOST, DB_USUARIO, DB_CONTRA, DB_NOMBRE);

			if($this->conexion_db->connect_errno){
				echo "Conexion No Establecida con el Host".$this->conexion_db->connect_error;

				return;
			}

			$this->conexion_db->set_charset(DB_CHARSET);
		}

	}
?>