<?php
    $imei= isset($_GET["imei"]) ? $_GET["imei"] : ''; 
    $fecha_ini= isset($_GET["fechaini"]) ? $_GET["fechaini"] : ''; 
    $fecha_fin= isset($_GET["fechafin"]) ? $_GET["fechafin"] : ''; 
?>

<html>
<head>
    <meta charset="UTF-8">
	<meta name="viewport" content="initial-scale=1.0">
  <meta http-equiv="x-ua-compatible" content="IE=edge">
    <title>Mapa</title>
	<style>
  	  #map {
        height: 100%;
      }

      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
	</style>
  <script src="js/jquery-2.1.0.js"></script>

  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
  integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
  crossorigin=""/>
  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
 integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
 crossorigin=""></script>


</head>
	<body>
        <input type="hidden" id="imei" value="<?php echo $imei; ?>">
        <input type="hidden" id="fecha_ini" value="<?php echo $fecha_ini; ?>">
        <input type="hidden" id="fecha_fin" value="<?php echo $fecha_fin; ?>">

		<div id ="map"></div>

        <script>
                navigator.geolocation.getCurrentPosition(position => {
                    console.log('lat:'+position.coords['latitude']);
                    console.log('lon:'+position.coords['longitude']);

                    lat = position.coords['latitude'];
                    lon = position.coords['longitude'];

                    CurrentPos(lat,lon);
                });

        function CurrentPos(lat,lon){
            var imei = $("#imei").val();
            var fecha_ini = $("#fecha_ini").val();
            var fecha_fin = $("#fecha_fin").val();
            var server = "3.129.11.64";
            var port = "80";

            var map = L.map('map').setView([lat, lon], 12);

            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: 'GPS Tracker'
            }).addTo(map);

            var GPSCli = L.icon({
                        iconUrl: 'assets/user_point.png',
                        iconSize: [38, 38]
                        });

            L.marker([lat,lon],{icon:GPSCli}).addTo(map);

            if(imei != ""){
                $.ajax("http://"+server+":"+port+"/public/api/gpstracker/historytracker?imei="+imei+"&fechaini="+fecha_ini+"&fechafin="+fecha_fin, {
                    contentType : 'application/json',
                    type : 'GET',
                    success(data) {
                        var response = JSON.parse(data);
                       // console.log(response);
                        
                        for(l in response){
                            var GPSCli = L.icon({
                             iconUrl: 'assets/gps_state.png',
                             iconSize: [38, 38]
                            });
                            L.marker([response[l].lat,response[l].lon],{icon:GPSCli}).addTo(map).bindPopup("Fecha: "+response[l].fecha+" Velocidad:"+response[l].speed+" Km/h").getPopup();
                            map.flyTo(new L.LatLng(response[l].lat,response[l].lon));
                        }

                    },
                    error(request, status, error) {
                    console.log(request)
                    }
                });
            }
        }

        
        </script>
	</body>
</html>